# [Current version of the course](https://gitlab.com/heidijcellis/cs-490-fa-2024)

# Welcome to CS 490 Fall 2023

The goal of this course is to prepare you to contribute to a real-world project. As such, you should think about the course more as the "start of your profession" than as a "course".  In other words, view CS 490 as your first steps into professional software development. As a result, I have professional expectations including:
- Things will change throughout the semester including the project content, assignments, and more. 
- You will attend all meetings/events early or on time. 
- You will communicate with your team mates and myself in a timely fashion. 
- I will not have all of the answers to your questions. 
- You will do significant learning outside of class. In other words, to find answers on your own. 
    - Yes, I will help you find answers, but you will have to do the learning independently. 
    - **Note:** You may find this frustrating. I would rather have you learn this skill now and be prepared for real-world software development than for you to learn via mistakes in your first position. 

## Before Class Starts:
We'll be working mostly in GitLab, using Kodiak only for some submissions. To get started: 

1. Read the [Syllabus.MD](https://gitlab.com/heidijcellis/cs-490-fa-2023/-/blob/main/Course%20Documents/Syllabus.MD) file
2. Read the [Schedule.MD](https://gitlab.com/heidijcellis/cs-490-fa-2023/-/blob/main/Course%20Documents/Schedule.MD) file
3. Complete the [Entrance Survey](https://forms.gle/eVbtHCF7eocjv8ke9) 
4. Read and follow the directions in the [Discord Guidelines - CS 490](https://gitlab.com/heidijcellis/cs-490-fa-2023/-/blob/main/Course%20Documents/Discord_Guidelines_-_CS_490.pdf) file


I look forward to seeing you in August!!

Heidi Ellis 


